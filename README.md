# work_planning_service

A small REST application, having main objective as a work planning service.
It is created in SLIM 3
You don't need to configure it, as it is already configure in repo.

# objective
The Business model of this application is: 
- A worker has shifts
- A shift is 8 hours long
- A worker never has two shifts on the same day
- It is a 24 hour timetable 0-8, 8-16, 16-24


# important Note!
- The database used in this project is mysql. You can check the database by going in database folder.
- For queries, I used **Prepared Statements**.
- All apis documentation can be found in **Api_Documentation** folder.


If you have any queries, u can ping me on **rehanraja25@gmail.com**
