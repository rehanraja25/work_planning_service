-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2022 at 09:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `work_planning`
--

-- --------------------------------------------------------

--
-- Table structure for table `lutbl_shifts`
--

CREATE TABLE `lutbl_shifts` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lutbl_shifts`
--

INSERT INTO `lutbl_shifts` (`id`, `shift_name`, `start_time`, `end_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Morning', '08:00:00', '16:00:00', 1, NULL, NULL),
(2, 'Evening', '16:00:00', '00:00:00', 1, NULL, NULL),
(3, 'Night', '00:00:00', '08:00:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `created_by`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 'ADMIN', 1, '2022-03-31 09:13:01', '2022-03-31 09:13:01'),
(2, 4, 'WORKER', 1, '2022-03-31 09:13:07', '2022-03-31 09:13:07'),
(3, 4, 'USER', 1, '2022-04-05 04:30:18', '2022-04-05 04:30:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_access`
--

CREATE TABLE `tbl_role_access` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_role_access`
--

INSERT INTO `tbl_role_access` (`id`, `role_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 1, '2022-03-31 09:13:17', '2022-03-31 09:13:17'),
(2, 2, 1, 1, '2022-04-01 07:13:03', '2022-04-01 07:13:03'),
(3, 3, 3, 1, '2022-04-05 04:30:37', '2022-04-05 04:30:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sessions`
--

CREATE TABLE `tbl_sessions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1:active,2:suspended,3:pending,4:deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_sessions`
--

INSERT INTO `tbl_sessions` (`id`, `user_id`, `session_token`, `session_data`, `session_ip`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'gp4qzDMMx0', '{\"Userid\":1,\"UserName\":\"rehan\",\"Password\":\"bbfe9feafa5d73fc93931d5da2461ee9\",\"Emai\":\"rehan@gmail.com\",\"Role\":\"\"}', '::1', 4, '2022-04-05 04:27:54', '2022-04-05 04:29:37'),
(2, 4, '4uhWDdb3py', '{\"Userid\":4,\"UserName\":\"hamza\",\"Password\":\"bbfe9feafa5d73fc93931d5da2461ee9\",\"Emai\":\"hamza@gmail.com\",\"Role\":\"\"}', '::1', 1, '2022-04-05 04:29:44', '2022-04-05 04:29:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shifts`
--

CREATE TABLE `tbl_shifts` (
  `id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL COMMENT 'shift created by',
  `shift_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_shifts`
--

INSERT INTO `tbl_shifts` (`id`, `shift_id`, `user_id`, `foreign_id`, `shift_desc`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 4, '', 1, '2022-04-01 15:32:14', '2022-04-01 15:32:14'),
(3, 1, 2, 4, '', 1, '2022-04-04 03:57:09', '2022-04-04 03:57:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shifts_dates`
--

CREATE TABLE `tbl_shifts_dates` (
  `id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL COMMENT 'it belongs to tbl_shifts',
  `user_id` int(11) NOT NULL,
  `shift_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_shifts_dates`
--

INSERT INTO `tbl_shifts_dates` (`id`, `shift_id`, `user_id`, `shift_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2022-04-01', 6, '2022-03-31 15:32:14', '2022-04-03 06:36:06'),
(2, 1, 1, '2022-04-02', 1, '2022-03-31 15:32:14', '2022-03-31 15:32:14'),
(3, 1, 1, '2022-04-03', 6, '2022-03-31 15:32:14', '2022-04-03 14:01:07'),
(4, 1, 1, '2022-04-04', 6, '2022-03-31 15:32:14', '2022-04-04 04:27:18'),
(5, 1, 1, '2022-04-05', 1, '2022-03-31 15:32:14', '2022-03-31 15:32:14'),
(8, 3, 2, '2022-04-09', 3, '2022-04-04 03:57:09', '2022-04-04 03:57:09'),
(9, 3, 2, '2022-04-10', 3, '2022-04-04 03:57:09', '2022-04-04 03:57:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shift_user_data`
--

CREATE TABLE `tbl_shift_user_data` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `shift_date_id` int(11) NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '5:started, 6:ended',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_shift_user_data`
--

INSERT INTO `tbl_shift_user_data` (`id`, `user_id`, `shift_id`, `shift_date_id`, `start_time`, `end_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL, 6, '2022-04-01 09:05:20', '2022-04-03 06:36:06'),
(3, 1, 1, 3, '08:00:00', '16:00:00', 6, '2022-04-03 13:56:31', '2022-04-03 14:01:07'),
(4, 1, 1, 4, '08:00:00', '16:00:00', 6, '2022-04-04 04:17:31', '2022-04-04 04:27:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:active,2:suspended,3:pending,4:deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `password`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rehan', 'rehan@gmail.com', 'bbfe9feafa5d73fc93931d5da2461ee9', 1, '2022-03-30 13:59:37', '2022-03-30 13:59:37'),
(2, 'abeel', 'abeel@gmail.com', 'bbfe9feafa5d73fc93931d5da2461ee9', 1, '2022-03-30 14:00:10', '2022-03-30 14:00:10'),
(3, 'ramiz', 'ramiz@gmail.com', 'bbfe9feafa5d73fc93931d5da2461ee9', 1, '2022-03-30 14:00:18', '2022-03-30 14:00:18'),
(4, 'hamza', 'hamza@gmail.com', 'bbfe9feafa5d73fc93931d5da2461ee9', 1, '2022-03-30 14:00:24', '2022-03-30 14:00:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lutbl_shifts`
--
ALTER TABLE `lutbl_shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_role_access`
--
ALTER TABLE `tbl_role_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_sessions`
--
ALTER TABLE `tbl_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_shifts`
--
ALTER TABLE `tbl_shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shit_id` (`shift_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `foreign_id` (`foreign_id`);

--
-- Indexes for table `tbl_shifts_dates`
--
ALTER TABLE `tbl_shifts_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_shift_user_data`
--
ALTER TABLE `tbl_shift_user_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_date_id` (`shift_date_id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lutbl_shifts`
--
ALTER TABLE `lutbl_shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_role_access`
--
ALTER TABLE `tbl_role_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_sessions`
--
ALTER TABLE `tbl_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_shifts`
--
ALTER TABLE `tbl_shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_shifts_dates`
--
ALTER TABLE `tbl_shifts_dates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_shift_user_data`
--
ALTER TABLE `tbl_shift_user_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_role_access`
--
ALTER TABLE `tbl_role_access`
  ADD CONSTRAINT `tbl_role_access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tbl_roles` (`id`),
  ADD CONSTRAINT `tbl_role_access_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_sessions`
--
ALTER TABLE `tbl_sessions`
  ADD CONSTRAINT `tbl_sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_shifts`
--
ALTER TABLE `tbl_shifts`
  ADD CONSTRAINT `tbl_shifts_ibfk_1` FOREIGN KEY (`shift_id`) REFERENCES `lutbl_shifts` (`id`),
  ADD CONSTRAINT `tbl_shifts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `tbl_shifts_ibfk_3` FOREIGN KEY (`foreign_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_shifts_dates`
--
ALTER TABLE `tbl_shifts_dates`
  ADD CONSTRAINT `tbl_shifts_dates_ibfk_1` FOREIGN KEY (`shift_id`) REFERENCES `tbl_shifts` (`id`),
  ADD CONSTRAINT `tbl_shifts_dates_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_shift_user_data`
--
ALTER TABLE `tbl_shift_user_data`
  ADD CONSTRAINT `tbl_shift_user_data_ibfk_1` FOREIGN KEY (`shift_id`) REFERENCES `tbl_shifts` (`id`),
  ADD CONSTRAINT `tbl_shift_user_data_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  ADD CONSTRAINT `tbl_shift_user_data_ibfk_3` FOREIGN KEY (`shift_date_id`) REFERENCES `tbl_shifts_dates` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
