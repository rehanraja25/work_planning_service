<?php require_once '../../apps/funcs.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en"><head><meta http-equiv=Content-Type content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Syntecx Solution</title>
<style type="text/css">body,td { font-family: arial, sans-serif; font-size: 13px } a:link,a:active { color: #1155CC; text-decoration: none }
a:hover { text-decoration: underline; cursor: pointer } a:visited { color: ##6611CC } img { border: 0px }
pre { white-space: pre; white-space: -moz-pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word; max-width: 800px; overflow: auto; }
</style></head><body>
	<table width=100% cellpadding=12 cellspacing=0 border=0>
		<tr>
			<td><div style="overflow: hidden;">
					<font size=-1>
						<table align="center" height="100%" width="100%" border="0"
							cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
							<tr>
								<td>
									<table align="center" height="100%" width="750px" border="0"
										cellspacing="0" cellpadding="0" bgcolor="#eeeeee"
										style="width: 750px !important">
										<tr>
											<td>
												<table width="690" align="center" border="0" cellspacing="0"
													cellpadding="0" bgcolor="#eeeeee">
													<tr>
														<td colspan="5" height="40" align="center" border="0"
															cellspacing="0" cellpadding="0" bgcolor="#eeeeee"
															style="padding: 0; margin: 0; font-size: 0; line-height: 0">&nbsp;</td>
												  </tr>
													<tr bgcolor="#ffffff">
														<td colspan="5" align="center">
															<table width="690" align="center" border="0"
																cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan="3" align="center" valign="top"
																		bgcolor="#8fb43e" width="690" height="166"
																		style="color: #ffffff; font-size: 30px; line-height: 40px; font-weight: normal; padding: 0; margin: 0"><img
																		src="http://www.synergyoftechnologies.com/Emailtemplates/Ufone/uBusiness/Header_image.jpg"
																		alt="get work done.. anywhere."></td>
															  </tr>
																<tr bgcolor="#C8C9CC">
																	<td width="30"></td>
																	<td height="40" align="right"><div>{DATE}</div></td>
																	<td width="30"></td>
																</tr>
																<tr>
																	<td colspan="3" height="42"></td>
															  </tr>
															</table>
														</td>
													</tr>
													<tr bgcolor="#ffffff">
														<td>
															<table width="630" align="center" border="0"
																cellspacing="0" cellpadding="0">
																<tr>
																	<td align="center" valign="top"><img src="http://www.synergyoftechnologies.com/Emailtemplates/Ufone/uBusiness/Mobile.jpg" width="120" height="163"></td>
																  <td width="30" style="border-right: 1px solid #e5e5e5"></td>
																	<td width="30"></td>
																	<td valign="top"
																		style="padding: 0; margin: 0; font-size: 1; line-height: 0">
																		<h3 style="color: #404040; font-size: 18px; line-height: 24px; font-weight: bold; padding: 0; margin: 0">Introducing Crew Business Android App</h3>
																		<div style="line-height: 10px; padding: 0; margin: 0"><br /></div>
																		<div style="color: #404040; font-size: 12px; line-height: 22px; font-weight: lighter; padding: 0; margin: 0">Crew Pakistan has been the trendsetter and market leader in location-based services in Pakistan. Crew Business has proven itself to be one of the most popular and useful corporate services for over 7 years. <br>
																			<br>
																			Crew now offers this popular service to android smartphone users through a specially designed app unleashing the proven power of Crew Business and portability of mobile devices.<br><br>
																			The app offers all features of Crew Business service through user-friendly interfaces while using very little device memory and battery resources.
                                                                            </div>
																		<div style="line-height: 22px; padding: 0; margin: 0"><br /></div>
																		<a href="https://play.google.com/store/apps/details?id=com.syntecx.ubussiness"
																		style="font-size: 18px; line-height: 24px; text-decoration: none; color: #f57921; font-weight: bold; padding: 0"
																		target="_blank">Get the Crew Business App today &nbsp;<img src="http://www.synergyoftechnologies.com/Emailtemplates/Ufone/uBusiness/arrow.png" height="10" />
																	</a>
																	</td>
																</tr>
																<tr>
																	<td colspan="4" height="42"></td>
															  </tr>
															</table>
														</td>
													</tr>
													<tr>
														<td bgcolor="#f9f9f9">
															<table width="630" align="center" border="0"
																cellspacing="0" cellpadding="0">
																<tr>
																	<td height="40"></td>
																</tr>
																<tr>
																	<td valign="top"
																		style="padding: 0; margin: 0; font-size: 1; line-height: 0">
																		<div
																			style="color: #808080; font-size: 14px; line-height: 22px; padding: 0; margin: 0">
																			Dear {SUBNAME},<br> <br> Please find attached location report for the last {HOUR} hours for your account:
																		</div>
																	</td>
																</tr>
																<tr>
																	<td height="20"></td>
																</tr>
																<tr>
																	<td bgcolor="#ffffff"
																		style="padding: 20px; margin: 0; border: 1px solid #eeeeee">
																		<div
																			style="color: #404040; font-size: 16px; line-height: 22px; padding: 0; margin: 0; border-radius: 3px">
																			<table width="100%" border="0" cellpadding="0"
																				cellspacing="0">
																				<tr>
																					<td width="50%" align="left" valign="middle">Start Time: {SDT}</td>
																					<td width="50%" align="right" valign="middle">End Time: {EDT}</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td
																		style="font-size: 0; line-height: 0; padding: 0; margin: 0"><img
																		src="http://www.synergyoftechnologies.com/Emailtemplates/box_arrow_down.png"
																		alt=""></td>
																</tr>
																<tr>
																	<td height="10"></td>
																</tr>
																<tr>
																	<td valign="top"
																		style="padding: 0; margin: 0; font-size: 1; line-height: 0">
																		<div style="color: #808080; font-size: 14px; line-height: 18px; padding: 0; margin: 0">
																		If you have any further queries regarding this report or Crew Business,
																		please contact your respective Crew Account Manager or contract Ufone Help line
																		by dialing 333 from your Crew Number.
																		</div>
																	</td>
																</tr>
																<tr>
																	<td height="20"></td>
																</tr>
																<tr>
																	<td valign="top"
																		style="padding: 0; margin: 0; font-size: 1; line-height: 0">
																		<div
																			style="color: #808080; font-size: 14px; line-height: 22px; padding: 0; margin: 0">
																			Kind Regards,<br>{OPERATOR_NAME} Crew Business
																		</div>
																	</td>
																</tr>
																<tr>
																	<td height="40"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr bgcolor="#ffffff">
														<td colspan="3" align="center"><a href="https://play.google.com/store/apps/details?id=com.syntecx.ubussiness" target="_blank">
															<img src="http://www.synergyoftechnologies.com/Emailtemplates/Ufone/uBusiness/Footer_Banner.jpg" width="690" height="180"></a>
														</td>
												  </tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="690" align="center" border="0" cellspacing="0"
										cellpadding="0" bgcolor="#eeeeee">
										<tr>
											<td colspan="2" height="30"></td>
										</tr>
										<tr>
											<td width="360" valign="top">
												<div
													style="color: #a3a3a3; font-size: 12px; line-height: 12px; padding: 0; margin: 0">
													Please note: This is a system generated email. <br />
													Please do not reply to it.
													</div>
												<div style="line-height: 5px; padding: 0; margin: 0"></div>
											</td>
											<td align="right" valign="top"><a target="_blank" href="http://www.syntecx.net/">
											<img src="http://www.synergyoftechnologies.com/Emailtemplates/Ufone/uBusiness/Powered.jpg" width="150" height="14"></a></td>
									  </tr>
										<tr>
											<td colspan="2" height="5"></td>
										</tr>
										<tr>
											<td>
										  </td>
											<td align="right">
												<span style="line-height: 20px; font-size: 20px">&nbsp;</span>
												<span style="line-height: 20px; font-size: 20px">&nbsp;</span>
												<span style="line-height: 20px; font-size: 20px">&nbsp;</span>
											</td>
									  </tr>
										<tr>
											<td colspan="2" height="80"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</font>
				</div>
			</td>
		</tr>
	</table>
</body></html>