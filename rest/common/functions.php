<?php 

/**
 * Validate token
 * @param number $iUserID
 * @param string $sToken
 * @return boolean
 */
function doValidateToken($iUserID, $sToken) {
    global $bDebug;
    $bValidToken = false;
    $objSess = new Sessions();
    $objSess->openConnection();
    if ($bDebug)
        $objSess->setDebug($bDebug);
    $objSess->setUserID($iUserID);
    $objSess->setSessionToken($sToken);
    $objSess->setStatus(OBJECT_STATUS_ACTIVE);
    $objSess->validateToken();
    if ($objSess->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS)
        $bValidToken = true;
    $objSess->closeConnection();
    return $bValidToken;
}




function fGetDTM($vPurpose = 0, $vDate = "") {
    $sDTM = "";
    if ($vDate == "") {
        $vDate = strtotime("now");
        $sElements = explode(" ", microtime()); //fPrintR($sElements);
        $vMicSec = round($sElements[0] * 1000);
        $vMicSec = str_pad($vMicSec, 4, "0", STR_PAD_LEFT);
    } else
        $vDate = strtotime($vDate);
    switch ($vPurpose) {
        default:
        case 0: $sDTM = date('YmdHis', $vDate);
            break; // Everything out
        case 1: $sDTM = date('YmdHis', $vDate) . $vMicSec;
            break; // Everything out w/ MilliSeconds
        case 2: $sDTM = date('M j, Y H:i:s', $vDate);
            break; // Best Printable
        case 3: $sDTM = date('M j, Y H:i:s.', $vDate) . $vMicSec;
            break; // Best Printable w/ MilliSeconds
        case 4: $sDTM = date('Y-m-d H:i:s', $vDate);
            break; // MySQL Style
        case 5: $sDTM = date('Y-m-d', $vDate);
            break; // MySQL Style - Date Only
        case 6: $sDTM = date('M j, y H:i', $vDate);
            break; // For Location in SMS
        case 7: $sDTM = date('jMy H:i', $vDate);
            break; // For Location in FT History SMS
        case 8: $sDTM = date('jS \of F Y, H:i', $vDate);
            break; // For CSR and Dashboard
        case 9: $sDTM = date('jS \of F Y, H:i:s', $vDate);
            break; // For CSR and Dashboard, with seconds
        case 10: $sDTM = date('jS \of F Y', $vDate);
            break; // For CSR and Dashboard, without Time
        case 11: $sDTM = date('l, j F Y', $vDate);
            break; // For datepicker widget display
        case 12: $sDTM = date('M Y', $vDate);
            break;
        case 13: $sDTM = date('Y-m-d H:i', $vDate);
            break; // MySQL Style - No Seconds
        case 14:   // Hour and the last tenth minute means 10 incase of 15 and 20 incase of 25 etc.
            $vTheHour = date('H', "last hour");
            $vTheMinute = date('i');
            $sTheHour = str_pad($vTheHour, 2, "0", STR_PAD_LEFT);
            $vTheMinute = $vTheMinute - ($vTheMinute % 10);
            $sTheMinute = str_pad($vTheMinute, 2, "0", STR_PAD_LEFT);
            $sDTM = $sTheHour . $sTheMinute;
            break;
        case 15:   // MySQL Style - Minutes is zero or 30 and Seconds is zero.
            if (date('i') > 30)
                $sDTM = date('Y-m-d H:30:00', strtotime("20 mins ago"));
            else
                $sDTM = date('Y-m-d H:00:00', strtotime("20 mins ago"));
            break;
        case 16: $sDTM = date('dmY', $vDate);
            break; // Date only for UFone Incremental Charging File Name.
        case 17:   // Best Printable - Minutes is zero or 30 and No Seconds.
            if (date('i') > 30)
                $sDTM = date('M j, Y H:30', strtotime("20 mins ago"));
            else
                $sDTM = date('M j, Y H:00', strtotime("20 mins ago"));
            break;
        case 18: $sDTM = date('j F Y h:i A', $vDate);
            break; // For display last login info.
        case 19: $sDTM = date('YmdHi', $vDate);
            break; // Everything out, minus the seconds.
        case 20: $sDTM = date('d', $vDate);
            break; // For Day of the month only (with leading zeros).
        case 21: $sDTM = date('Ymd', $vDate);
            break; // For only Year Month Day.
        case 22: $sDTM = date('d M, Y', $vDate);
            break;
        case 23: $sDTM = date('m-d-Y', $vDate);
            break;
        case 24: $sDTM = date('H:i:s', $vDate);
            break;
        case 25: $sDTM = date('M d', $vDate);
            break;
        case 26: $sDTM = date('Y', $vDate);
            break;
        case 27: $sDTM = date('M', $vDate);
            break;
        case 28: $sDTM = date('U');
            break;
        case 29: $sDTM = date('N', $vDate);
            break; // For Day of the Week only [1 (for Monday) through 7 (for Sunday)].
    }
    return $sDTM;
}