<?php

// LIVE SERVER
define("SERVER_PROTOCOL_TYPE", 'https');


define("API_SUCCESS_CODE", 1);
define("API_FAILED_CODE", 0);
define("API_SESSION_EXPIRE_CODE", 2);
define("API_SUCCESS_MESSAGE", "Success");
define("API_FAILED_MESSAGE", "Failed");
define("API_FAILED_CREDENTIAL", "Access denied!");
define("SESSION_EXPIRE", "Session Expires, Please Login again");
define("SESSION_ALREADY_EXISTS", "Session deletion Failed");


define("API_EMAIL_ALREADY_EXIST", "Email already Exists");
define("API_USER_NOT_FOUND", "User Not Register");
define("API_USER_LOGIN_SUCCESS_MESSAGE", "User logged in.");
define("API_USER_LOGIN_FAIL_MESSAGE", "Login failed!");
define("API_INVALID_CREDENTIAL_MESSAGE", "Invalid Email/Password!");

define("ROLE_ADMIN", "ADMIN");
define("ROLE_WORKER", "WORKER");

define("USER_SHIFT_NOT_AVAILABLE", 'No Shift available');
define("USER_SHIFT_ALREADY_STARTED", 'Shift already Started');
define("USER_HAS_NO_ROLE", 'Sorry! Cannot proceed as user has currently no role');
define("USER_ROLE_NOT_CORRECT", 'Sorry! Cannot proceed as user has wrong Role');
define("USER_ADD_SHIFT_DATE_FAILED", "Shift Date Add Failed");

define("USR_SHIFT_ALREADY_ENDED", 'Shift already Ended');
define("USER_SHIFT_ENDED_SUCCESS", 'Shift Ended Success');
define("USER_SHIFT_ENDED_FAILED", 'Shift Ended Failed');

define("API_USER_LOGOUT_SUCCESS_MESSAGE", "User logged out");
define("API_USER_LOGOUT_FAIL_MESSAGE", "Logout failed!");
define("API_USER_NO_ACTIVE_SESSION_MESSAGE", "No Active session!");
define("API_USER_NOT_ADMIN", "You cannot create Shift, Only Admin Can!");

