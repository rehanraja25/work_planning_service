<?php

//**********************************************************
// API Process Constants
//**********************************************************
define("USER_LOGIN", "USER_LOGIN");
define("USER_LOGOUT", "USER_LOGOUT");
define("USER_REGISTER", "USER_REGISTER");
define("USER_ROLE", "USER_ROLE");
define("ADD_ROLE", "ADD_ROLE");
define("ADD_ROLE_ACCESS", "ADD_ROLE_ACCESS");
define("LIST_ROLE", "LIST_ROLE");

define("ADD_SHIFT", "ADD_SHIFT");
define("LIST_SHIFTS", "LIST_SHIFTS");
define("START_SHIFT", "START_SHIFT");
define("END_SHIFT", "END_SHIFT");
define("LIST_USER_SHIFT", "LIST_USER_SHIFT");