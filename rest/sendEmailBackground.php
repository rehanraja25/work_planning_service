<?php
/**
 * Sending Email In Background Process
 */
define ("HOME_DIR", dirname(__FILE__));
include_once HOME_DIR.'/common/constants.php';
include_once HOME_DIR.'/vendor/emailService/emailservice.php';

$sEmailTo = $sEmailSubject = $sEmailBody = "";
$sEmailTypeKey = isset($_GET['type'])  ? trim($_GET['type']) : "";

switch ( $sEmailTypeKey ) {
	case EMAIL_FORGOT_PASSWORD:
		$sAgentName = isset($_GET['agentname'])  ? trim($_GET['agentname']) : "";
		$sNewPass = isset($_GET['pass'])  ? trim($_GET['pass']) : "";
		$sEmailBody  = "Dear {USER_NAME},<br/><br/>".chr(10);
		$sEmailBody .= "Your new password is:<br/>".chr(10);
		$sEmailBody .= "{NEW_PASSWORD}:<br/>".chr(10);
		$sEmailBody = str_replace(
		array("{USER_NAME}", "{NEW_PASSWORD}"),
		array($sAgentName, $sNewPass),
		$sEmailBody);
		$sEmailSubject = 'Forgot Password';
		break;
}

if(!empty($sEmailTo) && !empty($sEmailSubject) && !empty($sEmailBody))	{
	$sEmailSignature  = "<br/><br/><br/>".chr(10);
	$sEmailSignature .= "Thank You.<br/>".chr(10);
	$sEmailSignature .= COMPANY_NAME;
	$sEmailBody .= $sEmailSignature;
	sendEmail ( $sEmailTo, $sEmailSubject, $sEmailBody );
}