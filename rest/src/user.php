<?php

$iUserName = ($request->getParam('user_name')) ? $request->getParam('user_name') : "";
$iUserEmail = ($request->getParam('user_email')) ? $request->getParam('user_email') : "";
$iUserPassword = ($request->getParam('user_password')) ? $request->getParam('user_password') : "";
$iRoleName = ($request->getParam('role_name')) ? $request->getParam('role_name') : "";
$iRoleID = ($request->getParam('role_id')) ? $request->getParam('role_id') : "";
$iRoleForUser = ($request->getParam('role_for_user_id')) ? $request->getParam('role_for_user_id') : "";

$objUser = new User();
if (!$bConnectionOpen)
    $objConnection = $objUser->openConnection();
$objSes = new Sessions();
$objSes->setConnection($objConnection);
$objCommon = new Common();
$objCommon->setConnection($objConnection);

switch ($sProcessName) {
   
    case USER_LOGIN:
        if (empty($iUserEmail))
            $sValidationError = "User Email cannot be empty";
        else if (empty($iUserPassword))
            $sValidationError = "User Password cannot be empty";
        else{
            $isProceed = false;
            $objUser->setUserEmail($iUserEmail);
            $objUser->setUserPassword(md5($iUserPassword));
            $response = $objUser->listUserByEmail();
            if(count($response) > 0){
                $iUserID = $response[0]->id;
                $aUserName = $response[0]->name;
                $aUserPassword = $response[0]->password;
                //means user exist now check for session first
                $objSes->setStatus(OBJECT_STATUS_ACTIVE);
                $objSes->setUserID($iUserID);
                $isSession = $objSes->lastActivityHistoryOnPortal();
                if(count($isSession) > 0){
                    //means user has active session already so we first delete his previous action then proceed next
                    $sessionID = $isSession[0]->id;
                    $objSes->setSessionID($sessionID);
                    $objSes->setStatus(OBJECT_STATUS_DELETED);
                    $objSes->delete();
                    if ($objSes->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS)
                        $isProceed = true;
                }
                else
                    $isProceed = true;

                if($isProceed){
                    //get Role of user if he/she have

                    $objUser->setUserID($iUserID);
                    $aResponse =  $objUser->getRoleByUserID();
                    if(count($aResponse) > 0){
                        $aUserRole = $aResponse[0]->role_name;
                    }
                    else
                        $aUserRole = "";
                    
                    //now validate the password
                    if (md5($iUserPassword) == $aUserPassword){
                        $sToken = $objCommon->getRandomString();
                        $sData = json_encode(array("Userid" => $iUserID, "UserName" => $aUserName, "Password" => $aUserPassword, "Emai" => $iUserEmail, "Role" => $aUserRole));
    
                        $objSes->setSessionToken($sToken);
                        $objSes->setSessionIp(INCOMMING_IP);
                        $objSes->setSessionData($sData);
                        $objSes->setStatus(OBJECT_STATUS_ACTIVE);
                        $objSes->insert();
                        if ($objSes->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                             $iResCode = API_SUCCESS_CODE;
                            $sMessage = API_USER_LOGIN_SUCCESS_MESSAGE;
                            $aData = $aUserName . "|" . $iUserID . "|" . $sToken . "|" . $iUserEmail . "|" . $aUserRole . "|";
                        } else {
                            $sMessage = API_USER_LOGIN_FAIL_MESSAGE;
                        }
                    }
                    else{
                        $sMessage = API_INVALID_CREDENTIAL_MESSAGE;
                    }
                }
                else{
                    $sMessage = SESSION_ALREADY_EXISTS;
                }
            }
            else{
                $iResCode = API_FAILED_CODE;
                $sMessage = API_USER_NOT_FOUND;
            }
        }
    break;

    case USER_REGISTER:
        if (empty($iUserName))
            $sValidationError = "User Name cannot be empty";
        else if (empty($iUserEmail) && !filter_var($iUserEmail, FILTER_VALIDATE_EMAIL))
            $sValidationError = "User Email cannot be empty";
        else if (empty($iUserPassword))
            $sValidationError = "User Password cannot be empty";
        else{
            //first check if user is already register or not
            $response = $objCommon->checkIfUserExistOrNot($iUserEmail);
            if($response > 0){
                $iResCode = API_FAILED_CODE;
                $sMessage = API_EMAIL_ALREADY_EXIST;
            }
            else{
                $objUser->setUserName($iUserName);
                $objUser->setUserEmail($iUserEmail);
                $objUser->setUserPassword(md5($iUserPassword));
                $objUser->setStatus(OBJECT_STATUS_ACTIVE);
                $objUser->registerUser();
                if ($objUser->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                    $iResCode = API_SUCCESS_CODE;
                    $sMessage = API_SUCCESS_MESSAGE;
                }else{
                    $iResCode = API_FAILED_CODE;
                    $sMessage = API_FAILED_MESSAGE;
                }
            }
        }
    break;

    case ADD_ROLE:
        if (empty($iRoleName))
            $sValidationError = "Role Name cannot be empty";
        else{
            $isProceed = false;
            $objUser->setRoleName(strtoupper($iRoleName));
            $objUser->setUserID($iUserID);
            $objUser->setStatus(OBJECT_STATUS_ACTIVE);
            //check if user is admin
            $response = $objUser->getRoleByUserID();
            if(count($response) > 0){
                if($response[0]->role_name == ROLE_ADMIN)
                    $isProceed = true;
            }
            else {
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_HAS_NO_ROLE;
            }
            if($isProceed){
                $objUser->addRole();
                if ($objUser->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                    $iResCode = API_SUCCESS_CODE;
                    $sMessage = API_SUCCESS_MESSAGE;
                    $aData = $objUser->getLastInsertID();
                }else{
                    $iResCode = API_FAILED_CODE;
                    $sMessage = API_FAILED_MESSAGE;
                }
            }
            else{
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_ROLE_NOT_CORRECT;
            }
        }
    break;

    case ADD_ROLE_ACCESS:
        if (empty($iRoleID))
            $sValidationError = "RoleID cannot be empty";
        else if(empty($iRoleForUser))
            $sValidationError = "Role for User cannot be empty";
        else{
            $isProceed = false;
            $objUser->setUserID($iUserID);
            $objUser->setStatus(OBJECT_STATUS_ACTIVE);
            //check if user is admin
            $response = $objUser->getRoleByUserID();
            if(count($response) > 0){
                if($response[0]->role_name == ROLE_ADMIN)
                    $isProceed = true;
            }
            else {
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_HAS_NO_ROLE;
            }

            if($isProceed){
                $objUser->setRoleID($iRoleID);
                $objUser->setForeignID($iRoleForUser);
                $objUser->setStatus(OBJECT_STATUS_ACTIVE);
                $objUser->addRoleAccess();
                if ($objUser->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                    $iResCode = API_SUCCESS_CODE;
                    $sMessage = API_SUCCESS_MESSAGE;
                    $aData = $objUser->getLastInsertID();
                }else{
                    $iResCode = API_FAILED_CODE;
                    $sMessage = API_FAILED_MESSAGE;
                }
            }else{
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_ROLE_NOT_CORRECT;
            }
        }
    break;

    case LIST_ROLE:
        $objUser->setUserID($iUserID);
        $objUser->setStatus(OBJECT_STATUS_ACTIVE);
        $response = $objUser->getRoleByUserID();
        if(count($response) > 0){
            $iResCode = API_SUCCESS_CODE;
            $sMessage = API_SUCCESS_MESSAGE;
            $aData = $response;
        }
        else{
            $iResCode = API_FAILED_CODE;
            $sMessage = API_FAILED_MESSAGE;
        }
    break;

    case USER_LOGOUT:
        $objSes->setStatus(OBJECT_STATUS_ACTIVE);
        $objSes->setUserID($iUserID);
        $aUserHasSession = $objSes->checkUserSession();
        if($aUserHasSession > 0){
            //delete user session now
            $objSes->setStatus(OBJECT_STATUS_DELETED);
            $objSes->expireActiveSessionByUserID();
            if ($objSes->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                $iResCode = API_SUCCESS_CODE;
                $sMessage = API_USER_LOGOUT_SUCCESS_MESSAGE;
            } else {
                $sMessage = API_USER_LOGOUT_FAIL_MESSAGE;
            }
        }else{
            $sMessage = API_USER_NO_ACTIVE_SESSION_MESSAGE;
        }
    break;
}

if (!empty($sValidationError)) {
    $sMessage = $sValidationError;
    $sResLogDesc = $sValidationError;
}

if (!$bConnectionOpen)
    $objUser->closeConnection();