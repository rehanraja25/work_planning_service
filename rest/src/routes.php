<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
include_once HOME_DIR . '/common/constants.php';
include_once HOME_DIR . '/common/functions.php';


$bDebug = isset($_REQUEST['debug']) ? true : false;
$aAPIResponse = array(
    "rescode" => API_FAILED_CODE,
    "message" => "",
    "data" => array(),
    "extra" => array()
);

$app->get("/", "getRequest");
$app->post("/", "postRequest");

function getRequest($request, $res, $args) {
    global $aAPIResponse, $bDebug;
    if ($bDebug)
        postRequest($request, $res, $args);
    else
        echo "Get Request";
}

function postRequest($request, $res, $args) {
    global $aAPIResponse, $bDebug;
    $aData = array();
    $aExtra = array();
    $sMessage = $sToken = $sValidationError = $sResLogDesc = $sProcessName = "";
    $iResCode = $iUserID = 0;

    $aPostedData = array();
    if (isset($_POST) && count($_POST) > 0) {
        $sProcessName = strtoupper($_POST['process']);
        if($sProcessName)
            $aPostedData['process'] = isset($sProcessName);
        $aPostedData['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
    } else {
        $aPostedData = $request->getParsedBody();
        $sProcessName = strtoupper($aPostedData['process']);
        $aPostedData['user_id'] = isset($aPostedData['user_id']) ? $aPostedData['user_id'] : 0;
    }

    $aPostedData['x-access-token'] = 0;
    if ($request->hasHeader('x-access-token')) {
        $headers = $request->getHeader('x-access-token');
        $aPostedData['x-access-token'] = isset($headers[0]) ? $headers[0] : 0;
    }

    $aMandatoryFields = array("process" => "process", "x-access-token" => "x-access-token", "user_id" => "user_id");
    if (in_array($sProcessName, array(USER_LOGIN, USER_REGISTER))) {
        $aMandatoryFields = array();
    }

    
    $bMandatoryFieldsExist = true;
    foreach ($aMandatoryFields as $thisMF) {
        if (!array_key_exists($thisMF, $aPostedData)) {
            $sMessage = "Mandatory param missing";
            $sResLogDesc = $sMessage;
            $bMandatoryFieldsExist = false;
            break;
        }
    }

    if ($bMandatoryFieldsExist) {
        //***********************************
        //	Validate Token
        //***********************************
        $sToken = $aPostedData['x-access-token'];
        $iUserID = $aPostedData['user_id'];

        if (in_array($sProcessName, array(USER_LOGIN, USER_REGISTER)))
            $bValidToken = true;
        else
            $bValidToken = doValidateToken($iUserID, $sToken);

        //API Hit start from here if token is valid
        if ($bValidToken) {
            $bConnectionOpen = false;
            switch ($sProcessName) {
                //***********************************
                //	Api for Login/Register/Roles
                //***********************************
                case USER_LOGIN: case USER_REGISTER: case ADD_ROLE: case ADD_ROLE_ACCESS: case LIST_ROLE: case USER_LOGOUT:
                    include_once 'user.php';
                break;

                //***********************************
                //	Api for Shifts
                //***********************************
                case ADD_SHIFT: case LIST_SHIFTS: case START_SHIFT: case END_SHIFT: case LIST_USER_SHIFT:
                    include_once 'shifts.php';
                break;
            }
        }
        else {
            $iResCode = API_SESSION_EXPIRE_CODE;
            $sMessage = SESSION_EXPIRE;
        }
    }
    $aAPIResponse['rescode'] = $iResCode;
    $aAPIResponse['message'] = $sMessage;
    $aAPIResponse['data'] = $aData;
    $aAPIResponse['extra'] = $aExtra;

    return $res->withStatus(200)
    ->withHeader('Content-Type', 'application/json;charset=utf-8')
    ->write(json_encode($aAPIResponse) . " ");}