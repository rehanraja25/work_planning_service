<?php

$iShiftID = ($request->getParam('shift_id')) ? $request->getParam('shift_id') : "";
$iForeignID = ($request->getParam('foreign_id')) ? $request->getParam('foreign_id') : "";
$iShiftDates = ($request->getParam('shift_dates')) ? $request->getParam('shift_dates') : []; //sample will be like $shfitDays = array("2022-03-30, "2022-03-31", "2022-04-01", "2022-04-02", "2022-04-03")
$iShiftDesc = ($request->getParam('shift_desc')) ? $request->getParam('shift_desc') : "";
$iShiftUser = ($request->getParam('shift_user_id')) ? $request->getParam('shift_user_id') : "";
$iStartTime = ($request->getParam('start_time')) ? $request->getParam('start_time') : "";
$iEndTime = ($request->getParam('end_time')) ? $request->getParam('end_time') : "";

$objShift = new Shift();
if (!$bConnectionOpen)
    $objConnection = $objShift->openConnection();
$objUser = new User();
$objUser->setConnection($objConnection);
$objCommon = new Common();
$objCommon->setConnection($objConnection);


switch ($sProcessName) {

    //this case will list data from lutbl_shifts
    case LIST_SHIFTS:
        $objShift->setStatus(OBJECT_STATUS_ACTIVE);
        $aResponse = $objShift->listAll();
        if(count($aResponse) > 0){
            $iResCode = API_SUCCESS_CODE;
            $sMessage = API_SUCCESS_MESSAGE;
            $aData = $aResponse;
        }
        else{
            $iResCode = API_FAILED_CODE;
            $sMessage = API_FAILED_MESSAGE;
        }
    break;

    case ADD_SHIFT:
        if (empty($iForeignID))
            $sValidationError = "Shift Type ID Name cannot be empty";
        else if(empty($iShiftUser))
            $sValidationError = "Shift For User cannot be empty";
        else if(empty($iShiftDates))
            $sValidationError = "Shift Date cannot be empty";
        else{
            $isProceed = false;
            //first check if the user is admin or not. ONLY admin can create a shift
            $objUser->setUserID($iUserID);
            $objUser->setStatus(OBJECT_STATUS_ACTIVE);
            $aResponse = $objUser->getRoleByUserID();
            
            if(count($aResponse) > 0){
                if($aResponse[0]->role_name == ROLE_ADMIN)
                    $isProceed = true;                
            }

            if($isProceed){
                $isProceed = false;
                $objShift->setUserID($iShiftUser);
                $objShift->setStatus(OBJECT_STATUS_ACTIVE);
                //check if user has shift against the dates
                foreach ($iShiftDates as $thisData) {
                    $objShift->setShiftDate($thisData);
                    $aResponse = $objShift->checkUserShift(); //this will check if user has already shift on a specific date or not
                    if(count($aResponse) > 0){
                        $iResCode = API_FAILED_CODE;
                        $sMessage = "Shit already available";
                    }
                    else
                        $isProceed = true;
                }
            } else{
                $iResCode = API_FAILED_CODE;
                $sMessage = API_USER_NOT_ADMIN;
            }
            
            if($isProceed){
                //now add first data in tbl_shifts
                $objShift->setShiftDesc($iShiftDesc);
                $objShift->setForeignID($iForeignID);
                $objShift->setShiftForUser($iShiftUser);
                $objShift->setUserID($iUserID);
                $objShift->addShift();
                if ($objShift->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                    $lastInsertedID = $objShift->getLastInsertID();

                    //now its time to insert dates of shift
                    $objShift->setShiftID($lastInsertedID);
                    $objShift->setShiftDate($iShiftDates);
                    $objShift->setStatus(OBJECT_STATUS_PENDING);
                    $objShift->addShiftDates();

                    if ($objShift->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                        $iResCode = API_SUCCESS_CODE;
                        $sMessage = API_SUCCESS_MESSAGE;
                    }
                    else{
                        $iResCode = API_FAILED_CODE;
                        $sMessage = API_FAILED_MESSAGE;
                    }
                } else{
                    $iResCode = API_FAILED_CODE;
                    $sMessage = API_FAILED_MESSAGE;
                }
            }
        }
    break;

    case LIST_USER_SHIFT:
        $objShift->setStatus(OBJECT_STATUS_ACTIVE);
        $objShift->setUserID($iUserID);
        $aResponse = $objShift->listCurrentUserShifts();
        if(count($aResponse) > 0){
            foreach ($aResponse as $key => $thisData) {
                $aResponse[$key]["shift_dates"] = $objShift->getShiftDatesByShiftID($thisData['shift_id']);
            }
            $iResCode = API_SUCCESS_CODE;
            $sMessage = API_SUCCESS_MESSAGE;
            $aData = $aResponse;
        }
        else{
            $iResCode = API_FAILED_CODE;
            $sMessage = API_FAILED_MESSAGE;
        }
    break;

    case START_SHIFT:
        if (empty($iShiftID))
            $sValidationError = "ShiftID cannot be empty";
        else if(empty($iForeignID))//date id
            $sValidationError = "Shift Date ID cannot be empty";
        else if(empty($iStartTime))
            $sValidationError = "Shift Start Time cannot be empty";
        else{
            //check if user role is worker or not
            $isProceed = false;
            $objUser->setUserID($iUserID);
            $objUser->setStatus(OBJECT_STATUS_ACTIVE);
            $response = $objUser->getRoleByUserID();
            if(count($response) > 0){
                if($response[0]->role_name == ROLE_WORKER)
                    $isProceed = true;
            }
            else {
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_HAS_NO_ROLE;
            }
            if($isProceed){
                $isProceed = false;
                $objShift->setShiftID($iShiftID);
                $objShift->setUserID($iUserID);
                $objShift->setForeignID($iForeignID);
                //now check he is starting his current date shift
                $objShift->setStatus(OBJECT_STATUS_ACTIVE);
                $aResponse = $objShift->getShiftDatesByID($iForeignID);//date id
                if($aResponse > 0){
                    
                    $objShift->setStatus(OBJECT_STATUS_START);

                    //check the shift is already started or not
                    $sResponse = $objShift->checkShiftStatus();
                    if($sResponse > 0){
                        $iResCode = API_FAILED_CODE;
                        $sMessage = USER_SHIFT_ALREADY_STARTED;
                    }
                    else{
                        $objShift->setShiftStartTime($iStartTime);
                        $objShift->addUserStartShiftData();
                        if ($objShift->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                            $iResCode = API_SUCCESS_CODE;
                            $sMessage = API_SUCCESS_MESSAGE;
                            $aData = $objShift->getLastInsertID();
                        }
                        else{
                            $iResCode = API_FAILED_CODE;
                            $sMessage = USER_ADD_SHIFT_DATE_FAILED;
                        }
                    }
                }
                else{
                    $iResCode = API_FAILED_CODE;
                    $sMessage = USER_SHIFT_NOT_AVAILABLE;
                }
            }
            else{
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_ROLE_NOT_CORRECT;
            }
        }
    break;

    case END_SHIFT:
        if(empty($iForeignID))//id which I returned while starting the shift
            $sValidationError = "ID cannot be empty";
        else if(empty($iEndTime))
            $sValidationError = "Shift End Time cannot be empty";
        else{
            $isProceed = false;
            $objUser->setUserID($iUserID);
            $objUser->setStatus(OBJECT_STATUS_ACTIVE);
            $response = $objUser->getRoleByUserID();
            if(count($response) > 0){
                if($response[0]->role_name == ROLE_WORKER)
                    $isProceed = true;
            }
            else {
                $iResCode = API_FAILED_CODE;
                $sMessage = USER_HAS_NO_ROLE;
            }

            if($isProceed){
                $objShift->setForeignID($iForeignID);
                $objShift->setStatus(OBJECT_STATUS_START);
                $objShift->setUserID($iUserID);
                //check if shift is already ended or not
                $aResponse = $objShift->checkShiftStatusByID();
                if(count($aResponse) > 0 ){
                    //means u can end the shift
                    $shiftDateID = $aResponse[0]->shift_date_id;
                    $objShift->setStatus(OBJECT_STATUS_END);
                    $objShift->setShiftEndTime($iEndTime);
                    $objShift->updateShiftStatus();

                    if ($objShift->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                        $objShift->setForeignID($shiftDateID);
                        $objShift->updateShiftDatesStatus();
                        if ($objShift->getProcessExecutionStatus() == WorkPlanning::PROCESS_SUCCESS) {
                            $iResCode = API_SUCCESS_CODE;
                            $sMessage = USER_SHIFT_ENDED_SUCCESS;
                        }
                    }
                    else{
                        $iResCode = API_FAILED_CODE;
                        $sMessage = USER_SHIFT_ENDED_FAILED;
                    }
                }
                else{
                    $iResCode = API_FAILED_CODE;
                    $sMessage = USR_SHIFT_ALREADY_ENDED;
                }
            }
        }
    break;
}

if (!empty($sValidationError)) {
    $sMessage = $sValidationError;
    $sResLogDesc = $sValidationError;
}

if (!$bConnectionOpen)
    $objShift->closeConnection();