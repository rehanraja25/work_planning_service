<?php

set_time_limit(0);


define ("HOME_DIR", dirname(__FILE__));
//echo HOME_DIR;
require_once dirname(HOME_DIR).DIRECTORY_SEPARATOR.'BLL'.DIRECTORY_SEPARATOR.'load.php';

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}
require __DIR__ . '/vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/slimSettings/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/slimSettings/dependencies.php';

// Register routes
require __DIR__ . '/src/routes.php';

// Run app
$app->run();