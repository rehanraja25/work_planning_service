<?php
include_once 'appConstants.php';

/**
 * BASE CLASSES
 */
include_once 'classes/class.WorkPlanning.php';
include_once 'classes/class.Connection.php';

/**
 * MODULE CLASSES
*/
include_once 'classes/class.Sessions.php';
include_once 'classes/class.User.php';
include_once 'classes/class.Common.php';
include_once 'classes/class.Shift.php';

?>