<?php

class Sessions extends Connection {

	//************************************************
	//	SET PROPERTIES
	//************************************************

	function setSessionID( $val ) {
		$this->sessionId = $this->sanitize($val);
	}

	function setSessionToken( $val ) {
		$this->sessionToken = $this->sanitize($val);
	}

	function setSessionIp( $val ) {
		$this->sessionIp = $this->sanitize($val);
	}

	function setSessionData( $val ) {
		$this->sessionData = $this->sanitize($val);
	}


	//************************************************
	//	GET PROPERTIES
	//************************************************

	function getSessionID() {
		return $this->sessionId;
	}

	function getSessionToken() {
		return $this->sessionToken;
	}

	function getSessionIp() {
		return $this->sessionIp;
	}

	function getSessionData() {
		return $this->sessionData;
	}


	//************************************************
	//	Funtions
	//************************************************


	function insert() {
		$sQuery = "INSERT INTO ".WorkPlanning::TBL_SESSIONS." (user_id ,session_token, session_data, session_ip, status, created_at, updated_at)".chr(10);
		$sQuery .= " VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

		try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("isssiss",
                $this->getUserID(),
                $this->getSessionToken(),
                $this->getSessionData(),
                $this->getSessionIp(),
                $this->getStatus(),
                $this->nowDTM,
                $this->nowDTM
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("Session insert failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
	}

	function delete() {
		
		$this->getProcessExecutionStatus() == WorkPlanning::PROCESS_FAILED;
		$status = OBJECT_STATUS_ACTIVE;

		$sQuery = "UPDATE ".WorkPlanning::TBL_SESSIONS." SET ".chr(10);
		$sQuery .= "status = ?, ".chr(10);
		$sQuery .= "updated_at = ? ".chr(10);
		$sQuery .= "WHERE id = ? ".chr(10);
		$sQuery .= "AND status = ? ".chr(10);
		try {
			$vConn = $this->getConnection();
			$vStatement = $vConn->prepare($sQuery);
			$vStatement->bind_param("isii", $this->getStatus(), $this->nowDTM, $this->getSessionID(), $status);
			$vStatement->execute();
			$vStatement->close();
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
		} catch (Exception $except) {
			trigger_error("Update failed with Error: " . $except->getMessage() . " (" . $except->getCode() . ")", E_USER_ERROR);
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
		}
	}

	function lastActivityHistoryOnPortal() {
		$sQuery = "SELECT CONCAT( FLOOR(HOUR(TIMEDIFF(created_at, '" . $this->nowDTM . "')) / 24), ' days, ', MOD(HOUR(TIMEDIFF(created_at, '" . $this->nowDTM . "')), 24), ".chr(10);
		$sQuery .="' hours, ', MINUTE(TIMEDIFF(created_at, '" . $this->nowDTM . "')), ' minutes, ', SECOND(TIMEDIFF(created_at, '" .$this->nowDTM . "')), ' seconds') AS ActivityTime, session_ip AS IP".chr(10);
		$sQuery .= ",DATE_FORMAT(created_at, '%Y-%m-%d %h:%i:%s %p') AS loggedin".chr(10);
		$sQuery .= ",status, id".chr(10);
		$sQuery .= "FROM ".WorkPlanning::TBL_SESSIONS.chr(10);
		$sQuery .= "WHERE status = ? ".chr(10);
		$sQuery .= "AND user_id = ? ".chr(10);
		$sQuery .="ORDER BY id DESC LIMIT 1".chr(10);
		if ($this->debug)
			$this->printQuery($sQuery);
		
		try	{
			$vConn = $this->getConnection();
			$vStatement = $vConn->prepare($sQuery);
			$vStatement->bind_param("ii", $this->getStatus(),  $this->getUserID());
			$vStatement->execute();
			$vStatement->bind_result($sActivityTime, $sIP, $sLoggedIn, $sStatus, $sID);
			while ($vStatement->fetch())	{
				$aResult[] = (object) array(
					'id' => $sID,
					'activity_time' => $sActivityTime,
					'ip' => $sIP,
					'logged_in' => $sLoggedIn,
					'status' => $sStatus,
				);
			}
			$vStatement->close();
		}	catch (Exception $excepLocUpdate)	{
			trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
		}
		return $aResult;
	}

	public function expireActiveSessionByUserID() {

		$checkStatus = OBJECT_STATUS_ACTIVE;

		$sQuery = "UPDATE ".WorkPlanning::TBL_SESSIONS." SET ".chr(10);
		$sQuery .= "updated_at = ?, ".chr(10);
		$sQuery .= "status = ? ".chr(10);
		$sQuery .= "WHERE user_id = ? ".chr(10);
		$sQuery .= "AND status = ? ".chr(10);
		if ($this->debug)
			$this->printQuery($sQuery);
		
		try {
			$vConn = $this->getConnection();
			$vStatement = $vConn->prepare($sQuery);
			$vStatement->bind_param("siii", $this->nowDTM, $this->getStatus(), $this->getUserID(), $checkStatus);
			$vStatement->execute();
			$vStatement->close();
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
		} catch (Exception $except) {
			trigger_error("Update failed with Error: " . $except->getMessage() . " (" . $except->getCode() . ")", E_USER_ERROR);
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
		}
	}

	function checkUserSession() {

		$result = 0;

		$sQuery = "SELECT".chr(10);
		$sQuery .= "id, session_token, session_ip, session_data, status, created_at, updated_at".chr(10);
		$sQuery .= "FROM ".WorkPlanning::TBL_SESSIONS.chr(10);
		$sQuery .= "WHERE user_id = ? ".chr(10);
		$sQuery .= "AND status = ? ".chr(10);

		if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("ii", $this->getUserID(), $this->getStatus());
            $vStatement->execute();
			$vStatement->store_result();
            
            $result = $vStatement->num_rows;
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $result;
	}

	function validateToken() {
		$sQuery = "SELECT ".chr(10);
		$sQuery .= "id, session_token, status ".chr(10);
		$sQuery .= "FROM ".WorkPlanning::TBL_SESSIONS. chr(10);
		$sQuery .= " WHERE user_id = ?" . chr(10);
		$sQuery .= " AND session_token = ?".chr(10);
		$sQuery .= " AND status = ?".chr(10);

		if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("isi", 
				$this->getUserID(),
				$this->getSessionToken(),
				$this->getStatus(),
			);
			$vStatement->execute();
            $vStatement->store_result();
            $result = $vStatement->num_rows;
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
		if($result > 0)
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
		else
			$this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
	}

}

?>