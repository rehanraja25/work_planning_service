<?php

class Shift extends Connection{

    //************************************************
	//	SET PROPERTIES
	//**

	function setShiftID($val) {
        $this->shift_id = $this->sanitize($val);
    }

    function setShiftForUser($val) {
        $this->for_user = $this->sanitize($val);
    }

    function setShiftDesc($val) {
        $this->shift_desc = $this->sanitize($val);
    }

    function setShiftDate($val) {
        $this->shiftDates = $val;
    }

    function setShiftStartTime($val) {
        $this->start_time = $val;
    }

    function setShiftEndTime($val) {
        $this->end_time = $val;
    }


    //************************************************
	//	GET PROPERTIES
	//************************************************

	function getShiftID() {
		return $this->shift_id;
    }

    function getShiftForUser() {
        return $this->for_user;
    }

    function getShiftDesc() {
        return $this->shift_desc;
    }

    function getShiftDate() {
        return $this->shiftDates;
    }

    function getShiftStartTime() {
        return $this->start_time;
    }

    function getShiftEndTime() {
        return $this->end_time;
    }


	//************************************************
	//	Functions
	//************************************************


	public function listAll(){
		$sQuery = "SELECT" . chr(10);
        $sQuery .= "id, shift_name, start_time, end_time" . chr(10);
        $sQuery .= " FROM " . WorkPlanning::LUTBL_SHIFTS . chr(10);
        $sQuery .= " WHERE status = ?" . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("i", $this->getStatus());
            $vStatement->execute();
            $vStatement->bind_result($sId, $sShiftName, $sStartTime, $sEndTime);
            while ($vStatement->fetch())	{
                $aResult[] = (object) array(
                    'id' => $sId,
                    'shit_name' => $sShiftName,
                    'start_time' => $sStartTime,
                    'end_time' => $sEndTime,
                );
            }
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("LookUp shift table Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
	}


	public function checkUserShift(){
		$sQuery = "SELECT" . chr(10);
        $sQuery .= "shift.id as shift_id, dates.id as shift_date_id " . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_SHIFTS . " shift " .chr(10);
		$sQuery .= "LEFT JOIN ". WorkPlanning::TBL_SHIFTS_DATES. " dates " .chr(10);
		$sQuery .= "ON shift.id = dates.shift_id" .chr(10);

        $sQuery .= " WHERE shift.user_id = ? and shift.status = ? and dates.status = ? and dates.shift_date <> ?" . chr(10);
		
        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iiis", $this->getUserID(), $this->getStatus(), $this->getStatus(), $this->getShiftDate());
            $vStatement->execute();
            $vStatement->bind_result($sShiftID, $sShiftDateID);
            while ($vStatement->fetch())	{
                $aResult[] = (object) array(
                    'shift_id' => $sShiftID,
                    'shift_date_id' => $sShiftDateID,
                );
            }
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("shift table Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
	}

	public function addShift(){

		$sQuery = "INSERT INTO `" . WorkPlanning::TBL_SHIFTS . "`(" . chr(10);
        $sQuery .= "shift_id, user_id, foreign_id, shift_desc, status, created_at, updated_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iiisiss",
                $this->getForeignID(),
				$this->getShiftForUser(),
                $this->getUserID(),
				$this->getShiftDesc(),
                $this->getStatus(),
                $this->nowDTM,
                $this->nowDTM
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("Shift Add failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
	}

	public function addShiftDates(){

		$this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
		$dates = $this->getShiftDate();
		$sQuery = "INSERT INTO `" . WorkPlanning::TBL_SHIFTS_DATES . "`(" . chr(10);
        $sQuery .= "shift_id, user_id, shift_date, status, created_at, updated_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
			foreach ($dates as $key => $value) {
				$vStatement->bind_param("iisiss",
					$this->getShiftID(),
					$this->getShiftForUser(),
					$value,
					$this->getStatus(),
					$this->nowDTM,
					$this->nowDTM
            	);
            	$vStatement->execute();
			}

            $vStatement->close();
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("ShiftDate Add failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
	}

    public function listCurrentUserShifts(){
        
        $sQuery = "SELECT" . chr(10);
        $sQuery .= "shift.id as shift_id, shift.shift_desc, " . chr(10);
        $sQuery .= "lutbl_shift.shift_name, lutbl_shift.start_time, lutbl_shift.end_time" . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_SHIFTS . " shift " .chr(10);
        $sQuery .= "LEFT JOIN ". WorkPlanning::LUTBL_SHIFTS. " lutbl_shift " .chr(10);
		$sQuery .= "ON shift.shift_id = lutbl_shift.id" .chr(10);

        $sQuery .= " WHERE shift.user_id = ? and shift.status = ? and  lutbl_shift.status = ? GROUP BY shift.id" . chr(10);
		
        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iii", $this->getUserID(), $this->getStatus(), $this->getStatus());
            $vStatement->execute();
            $vStatement->bind_result($sShiftID, $sShiftDesc, $sShiftName, $sShiftStartTime, $sShiftEndTime);
            while ($vStatement->fetch())	{
                $aResult[] =  array(
                    'shift_id' => $sShiftID,
                    'shift_desc' => $sShiftDesc,
                    'shift_name' => $sShiftName,
                    'start_time' => $sShiftStartTime,
                    'end_time' => $sShiftEndTime,
                );
            }
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
    }


    public function getShiftDatesByShiftID($id){

        $aResult = array();
        $sQuery = "SELECT" . chr(10);
        $sQuery .= "id, shift_date " . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_SHIFTS_DATES  .chr(10);
        $sQuery .= " WHERE shift_id = ? and status = ? " . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("ii", $id, $this->getStatus());
            $vStatement->execute();
            $vStatement->bind_result($sID, $sShiftDate);
            while ($vStatement->fetch())	{
                $aResult[] = (object) array(
                    'id' => $sID,
                    'shift_date_id' => $sShiftDate,
                );
                //array_push($aResult, $sShiftDate);
            }
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
    }

    public function addUserStartShiftData(){

		$this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
		$sQuery = "INSERT INTO `" . WorkPlanning::TBL_SHIFTS_USER_DATA . "`(" . chr(10);
        $sQuery .= " user_id, shift_id, shift_date_id, start_time, status, created_at, updated_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
			$vStatement->bind_param("iiisiss",
				$this->getUserID(),
				$this->getShiftID(),
				$this->getForeignID(),
                $this->getShiftStartTime(),
				$this->getStatus(),
				$this->nowDTM,
				$this->nowDTM
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("Start Shift Add failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
	}

    public function getShiftDatesByID($id){

        $todayDate = fGetDTM(5);
        $result = 0;
        $sQuery = "SELECT" . chr(10);
        $sQuery .= "id, shift_date " . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_SHIFTS_DATES  .chr(10);
        $sQuery .= " WHERE id = ? and shift_id = ? and user_id = ? and status = ? and shift_date = ?  " . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iiiis", $id, $this->getShiftID(), $this->getUserID(), $this->getStatus(), $todayDate);
            $vStatement->execute();
            $vStatement->store_result();
            
            $result = $vStatement->num_rows;
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $result;
    }

    public function checkShiftStatus(){
        
        $result = 0;

        $sQuery = "SELECT" . chr(10);
        $sQuery .= "id " . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_SHIFTS_USER_DATA  .chr(10);
        $sQuery .= " WHERE user_id = ? and shift_id = ? and shift_date_id = ? and status = ? " . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iiii", $this->getUserID(), $this->getShiftID(), $this->getForeignID(), $this->getStatus());
            $vStatement->execute();
            $vStatement->store_result();
            
            $result = $vStatement->num_rows;
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $result;
    }

    public function checkShiftStatusByID(){
        
        $result = 0;

        $sQuery = "SELECT" . chr(10);
        $sQuery .= "id, shift_date_id " . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_SHIFTS_USER_DATA  .chr(10);
        $sQuery .= " WHERE id = ? and user_id = ? and status = ? " . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iii", $this->getForeignID(), $this->getUserID(), $this->getStatus());
            $vStatement->execute();
            $vStatement->bind_result($sID, $sShiftDateID);

            while ($vStatement->fetch())	{
                $aResult[] = (object) array(
                    'id' => $sID,
                    'shift_date_id' => $sShiftDateID,
                );
            }
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
    }

    public function updateShiftStatus(){
        
        $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);

        $sQuery = " UPDATE ". WorkPlanning::TBL_SHIFTS_USER_DATA . " SET " .chr(10);
        $sQuery .= " end_time = ?," . chr(10);
        $sQuery .= " status = ?," . chr(10);
        $sQuery .= " updated_at = ?" . chr(10);
        $sQuery .= " WHERE id = ?" . chr(10);
        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("sisi", $this->getShiftEndTime(), $this->getStatus(), $this->nowDTM, $this->getForeignID());
            $vStatement->execute();
            $vStatement->close();
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $except) {
            trigger_error("Update failed with Error: " . $except->getMessage() . " (" . $except->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
    }

    public function updateShiftDatesStatus(){
        
        $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);

        $sQuery = " UPDATE ". WorkPlanning::TBL_SHIFTS_DATES . " SET " .chr(10);
        $sQuery .= " status = ?," . chr(10);
        $sQuery .= " updated_at = ?" . chr(10);
        $sQuery .= " WHERE id = ?" . chr(10);

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("isi", $this->getStatus(), $this->nowDTM, $this->getForeignID());
            $vStatement->execute();
            $vStatement->close();
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $except) {
            trigger_error("Update failed with Error: " . $except->getMessage() . " (" . $except->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
    }
}