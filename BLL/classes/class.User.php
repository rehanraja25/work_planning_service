<?php

class User extends Connection {


    //************************************************
	//	SET PROPERTIES
	//************************************************


    function setUserName($val) {
        $this->user_name = $this->sanitize($val);
    }

    function setUserPassword($val) {
        $this->user_password = $this->sanitize($val);
    }

    function setUserEmail($val) {
        $this->userEmail = $this->sanitize($val);
    }

    function setRoleName($val) {
        $this->role_name = $val;
    }

    function setRoleID($val) {
        $this->role_id = $val;
    }



    //************************************************
	//	GET PROPERTIES
	//************************************************

    function getUserName() {
        return $this->user_name;
    }

    function getUserEmail() {
        return $this->userEmail;
    }

    function getUserPassword() {
        return $this->user_password;
    }

    function getRoleName() {
        return $this->role_name;
    }

    function getRoleID() {
        return $this->role_id;
    }




    //************************************************
	//	Functions
	//************************************************


    function registerUser(){

        $sQuery = "INSERT INTO `" . WorkPlanning::TBL_USERS . "`(" . chr(10);
        $sQuery .= "name ,email, password, status, created_at, updated_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("sssiss",
                $this->getUserName(),
                $this->getUserEmail(),
                $this->getUserPassword(),
                $this->getStatus(),
                $this->nowDTM,
                $this->nowDTM
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("User Registration failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
    }

    function listUserByEmail(){

        $sQuery = "SELECT" . chr(10);
        $sQuery .= "id, name, password" . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_USERS . chr(10);
        $sQuery .= " WHERE email = ?" . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("s", $this->getUserEmail());
            $vStatement->execute();
            $vStatement->bind_result($sUserId, $sUserName, $sUserPassword);
            while ($vStatement->fetch())	{
                $aResult[] = (object) array(
                    'id' => $sUserId,
                    'name' => $sUserName,
                    'password' => $sUserPassword
                );
            }
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
    }

    function addRole(){

        $sQuery = "INSERT INTO `" . WorkPlanning::TBL_ROLE . "`(" . chr(10);
        $sQuery .= "name, created_by, status, created_at, updated_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("siiss",
                $this->getRoleName(),
                $this->getUserID(),
                $this->getStatus(),
                $this->nowDTM,
                $this->nowDTM
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("Role Add failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
    }

    function addRoleAccess(){

        $sQuery = "INSERT INTO `" . WorkPlanning::TBL_ROLE_ACCESS . "`(" . chr(10);
        $sQuery .= "role_id, user_id, status, created_at, updated_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("siiss",
                $this->getRoleID(),
                $this->getForeignID(),
                $this->getStatus(),
                $this->nowDTM,
                $this->nowDTM
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(WorkPlanning::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("RoleAccess Add failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
    }

    function getRoleByUserID(){

        $aResult = [];
        $sQuery = "SELECT" . chr(10);
        $sQuery .= "role.name" . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_ROLE . " role " .chr(10);
        $sQuery .= "LEFT JOIN ". WorkPlanning::TBL_ROLE_ACCESS. " access " .chr(10);
        $sQuery .= "ON role.id = access.role_id" .chr(10);
        $sQuery .= " WHERE access.user_id = ? AND access.status = ? AND role.status = ? LIMIT 1" . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("iii", $this->getUserID(), $this->getStatus(), $this->getStatus());
            $vStatement->execute();
            $vStatement->bind_result($sRoleName);
            while ($vStatement->fetch())	{
                $aResult[] = (object) array(
                    'role_name' => $sRoleName,
                );
            }
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $aResult;
    }
}
