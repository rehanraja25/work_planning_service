<?php

class Connection extends WorkPlanning {

    private $_host = "localhost";
    private $_db = "work_planning";
    private $_dbUser = "rehan";
    private $_dbPass = "P7E4mUowbmX8urpw";
    
    private static $_instance; //The single instance
    private $_connection = null;

    function __construct() {
        WorkPlanning::__construct();
    }

    function __destruct() {
        
    }

    /**
      Get an instance of the Database
      @return Instance
     */
    public static function getInstance() {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function setConnection($objConnection) {
        $this->_connection = $objConnection;
    }

    public function getConnection() {
        return $this->_connection;
    }

    public function openConnection() {
        $shost = $this->_host;
        $iUser = $this->_dbUser;
        $sPass = $this->_dbPass;
        $sdb = $this->_db;
        try {
            $this->_connection = new mysqli($shost, $iUser, $sPass, $sdb);
            if (mysqli_connect_error()) {
                trigger_error("DATABASE CONNECTION FAILED: " . mysqli_connect_error(), E_USER_ERROR);
            }
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            /* change character set to utf8 */
            if (!$this->_connection->set_charset("utf8")) {
                trigger_error("Error loading character set utf8: " . $this->_connection->error . "\n", E_USER_ERROR);
                exit();
            }
            //$this->_connection->query("SET NAMES utf8");
            $this->_connection->query("SET SESSION sql_mode = ''");
            return $this->_connection;
        } catch (Exception $e) {
            $this->throwException($e);
        }
    }

    public function closeConnection() {
        if (!empty($this->_connection))
            mysqli_close($this->_connection);
    }

    private function throwException($e) {
        die('Exception : ' . $e->getMessage());
    }

    public function sanitize($val) {
        if (is_array($val) || is_object($val))
            return $val;
        if ($val != 0)
            return $this->_connection->real_escape_string($val);
        else
            return $val;
    }
}

?>
