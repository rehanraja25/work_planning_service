<?php

/**
 * Base class
*/
class WorkPlanning {

	protected $debug = false;

	//***************************************
	// CONSTANTS
	//***************************************
	const PROCESS_SUCCESS = 1;
	const PROCESS_FAILED = 2;
	const debug = false;
    
	//	Modules
	const TBL_USERS = "tbl_users";
	const TBL_SESSIONS = "tbl_sessions";
	const TBL_ROLE = "tbl_roles";
	const TBL_ROLE_ACCESS = "tbl_role_access";
	const LUTBL_SHIFTS = "lutbl_shifts";
	const TBL_SHIFTS = "tbl_shifts";
	const TBL_SHIFTS_DATES = "tbl_shifts_dates";
	const TBL_SHIFTS_USER_DATA = "tbl_shift_user_data";

	
	function __construct() {
		$this->nowDTM = fGetDTM(4);
	}

	function __destruct() {}


	function printArray($aArray, $sName = "Array")	{
		echo "{$sName} => ".count($aArray)." Values<pre>" . print_r($aArray, true) . "</pre><hr>";
	}

	public function printQuery($sQuery)	{	echo "<pre><b>$sQuery</b></pre><hr>";	}

	public function fPrintR($aArray, $sName = "")	{
		if ($sName != "")	echo "Array '$sName' ";
		$theCount = count($aArray);
		echo "has (" . $theCount . " elements): -<br />";
		if ($theCount > 0)	echo "<pre>" . print_r($aArray, true) . "</pre><br />";
	}

	/**
	 * Encrypt password with SHA1
	 * @param string $val
	 * @return string
	 */
	public function encryptPassword($val)	{	return hash("sha256", $val);	}


	public function enableDebug()	{	$this->debug = true;	}

	public function disableDebug()	{	$this->debug = false;	}

	public function setDebug($val)	{	$this->debug = $val;	}

	public function getDebug()	{	return $this->debug;	}

	//************************************************
	//	SET PROPERTIES
	//************************************************

	function setUserID( $val ) {
		$this->userId = $val;
	}
	
	public function setLastInsertID($val) {
		$this->insertedId = $val;
	}

	public function setStatus($val) {
		$this->status = $val;
	}

	public function setRecordID($val) {
		$this->id = $val;
	}

	public function setRecordStatus($val) {
		$this->record_status = $val;
	}

	function setNowDTM() {
		$this->nowDTM = fGetDTM(4);
	}

	public function setProcessExecutionStatus($value) {
		$this->processStatus = $value;
	}

	function setForeignID($val) {
        $this->foreign_id = $val;
    }
	
	//************************************************
	//	GET PROPERTIES
	//************************************************

	function getUserID() {
		return $this->userId;
	}
	
	public function getLastInsertID() {
		return $this->insertedId;
	}

	public function getStatus() {
		return $this->status;
	}

	public function getRecordID() {	
		return $this->id;
	
	}

	public function getRecordStatus() {
		return $this->record_status;
	}

	public function getProcessExecutionStatus() {
		return $this->processStatus;
	}

	function getNowDTM() {
		return $this->nowDTM;
	}

	function getForeignID() {
        return $this->foreign_id;
    }

}
