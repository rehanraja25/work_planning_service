<?php

class Common extends Connection {
    
    function checkIfUserExistOrNot($email){

        $result = 0;
        $sQuery = "SELECT" . chr(10);
        $sQuery .= " id " . chr(10);
        $sQuery .= " FROM " . WorkPlanning::TBL_USERS . chr(10);
        $sQuery .= " WHERE email = ?" . chr(10);

        if ($this->debug)
            $this->printQuery($sQuery);
        try	{
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("s", $email);
            $vStatement->execute();
            $vStatement->store_result();
            
            $result = $vStatement->num_rows;
            $vStatement->close();
        }	catch (Exception $excepLocUpdate)	{
            trigger_error("Listing failed with Error: ".$excepLocUpdate->getMessage()." (".$excepLocUpdate->getCode().")", E_USER_ERROR);
            $this->setProcessExecutionStatus(WorkPlanning::PROCESS_FAILED);
        }
        return $result;
    }

    function getRandomString($length = PASSWORD_MIN_LENGTH, $ReqAlpha = true, $vReqNumric = true, $vReqSpecialChar = false) {
		$sCharactersAlpha = "abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
		$sCharactersNumeric = "0123456789";
		$sCharactersSpecial = "~!@#%&^*()_{}[]<>?$,+-/.`=";
		$sCharacters = '';
		if ($ReqAlpha) $sCharacters .= $sCharactersAlpha;
		if ($vReqNumric) $sCharacters .= $sCharactersNumeric;
		if ($vReqSpecialChar) $sCharacters .= $sCharactersSpecial;
		$sRandomString = '';	$strSize = strlen($sCharacters);
		for ($i = 0; $i < $length; $i++)	{
			$sRandomString .= $sCharacters[rand(0, $strSize - 1)];
		}
		return $sRandomString;
	}
}