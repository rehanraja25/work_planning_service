<?php
/**
 * Object Status Types
 */
define("OBJECT_STATUS_ACTIVE", 1);
define("OBJECT_STATUS_DISABLED", 2);
define("OBJECT_STATUS_PENDING", 3);
define("OBJECT_STATUS_DELETED", 4);
define("OBJECT_STATUS_START", 5);
define("OBJECT_STATUS_END", 6);

define("PASSWORD_MIN_LENGTH", 10);

define("INCOMMING_IP", isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 0);

?>
